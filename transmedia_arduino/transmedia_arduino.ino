const int buttonYes = 5;
const int buttonNo = 6;

boolean yesCurrentState = 0;
boolean yesLastState = 0;
boolean noCurrentState = 0;
boolean noLastState = 0;

void setup() {
  pinMode(buttonYes, INPUT_PULLUP);
  pinMode(buttonNo, INPUT_PULLUP);

  Serial.begin(9600);
  Serial.println("Ready");
}

void loop() {
  yesCurrentState = digitalRead(buttonYes);
  noCurrentState = digitalRead(buttonNo);

  handleButton(noCurrentState, noLastState, 'n');
  handleButton(yesCurrentState, yesLastState, 'y');
}

void handleButton(boolean &currentState, boolean &lastState, char button) {
  if (currentState != lastState) {
    if (currentState == HIGH) {
      if (lastState == LOW) {
        lastState = HIGH;
      }
    } else {
      if (lastState == HIGH) {
        lastState = LOW;
        Serial.println(button);
      }
    }
  }
  delay(50);
}
