class SubsLoader {
  JSONArray subs = loadJSONArray("sindica_subs.json");
  int[] time;
  String[] text;

  int subsSize;
  int currentSub;
  int firstSubTime;

  SubsLoader() {
    currentSub = 0;
    firstSubTime = -1;

    subsSize = subs.size();

    time = new int[subs.size()];
    text = new String[subs.size()];

    for (int i = 0; i < subs.size(); i++) {
      JSONObject data = subs.getJSONObject(i);

      time[i] = data.getInt("time");
      text[i] = data.getString("text");
    }
  }

  int getTime(int s) {
    return time[s];
  }

  String getText(int s) {
    return text[s];
  }

  int getCurrentSub() {
    return currentSub;
  }

  int getFirstSubTime() {
    return firstSubTime;
  }

  int getSubsSize() {
    return subsSize;
  }

  void setCurrentSub(int currentSub) {
    this.currentSub = currentSub;
  }

  void setFirstSubTime (int firstSubTime) {
    this.firstSubTime = firstSubTime;
  }
}
