import deadpixel.keystone.*;
import ddf.minim.*;
import processing.video.*;
import processing.serial.*;

Movie tutorial;

Serial serialPort;
String serialString = null;
boolean serialReady; 
static final int LF = 10;    // Linefeed in ASCII

Minim minim;
int steps = 6;
int trys = 3;
AudioPlayer[][] audios = new AudioPlayer[6][3];
AudioPlayer[] endings = new AudioPlayer[2];
AudioPlayer confirm;

Keystone ks;
CornerPinSurface mapSurface, photoSurface, hernanSurface, lucianoSurface, subtitleSurface;
PGraphics map, photo, hernan, luciano, subtitle;

String currentSubtitle;

SubsLoader SindicaSubs;

Portrait ph, pl;

PFont font;

String lastPressed;
Scanner scanner;

Main main;

void setup() {
  size(1920, 1080, P3D);
  
  SindicaSubs = new SubsLoader();

  tutorial = new Movie(this, "tutorial.mp4");
  tutorial.frameRate(24);

  font = createFont("data/font/courier_prime.ttf", 48);
  currentSubtitle = "holi";

  ph = new Portrait(ORANGE);
  pl = new Portrait(GREEN);


  minim = new Minim(this);
  for (int i = 0; i < steps; i++) {
    for (int j = 0; j < trys; j++) {
      println("audio/" + i + '/' + j + ".wav");
      audios[i][j] = minim.loadFile("audio/" + i + '/' + j + ".wav");
    }
  }

  endings[0] = minim.loadFile("audio/ending/bad.wav");
  endings[1] = minim.loadFile("audio/ending/good.wav");
  confirm = minim.loadFile("audio/confirm.wav");

  ks = new Keystone(this);
  mapSurface = ks.createCornerPinSurface(1684, 1786, 20);
  map = createGraphics(1684, 1786, P2D);
  photoSurface = ks.createCornerPinSurface(640, 480, 20);
  photo = createGraphics(640, 480, P2D);
  hernanSurface = ks.createCornerPinSurface(200, 300, 20);
  hernan = createGraphics(200, 300, P2D);
  lucianoSurface = ks.createCornerPinSurface(200, 300, 20);
  luciano = createGraphics(200, 300, P2D);
  subtitleSurface = ks.createCornerPinSurface(1600, 300, 20);
  subtitle = createGraphics(1600, 300, P2D);

  //ks.load();

  main = new Main(map.width, map.height);
  scanner = new Scanner();

  // Basic settings
  //noCursor();
  noStroke();
  textSize(16);
  textAlign(LEFT, TOP);

  // Serial
  String port = findPort();
  if (port != null) {
    serialPort = new Serial(this, Serial.list()[0], 9600);
  }
}

void update() {
  readSerial();
}

void draw() {
  update();
  main.update();

  //PVector surfaceMouse = mapSurface.getTransformedMouse();

  // Map
  map.beginDraw();
  if (DEBUGGING) {
    map.background(255);
  } else {
    map.background(0);
  }
  main.render(map);
  //main.police.setPos(surfaceMouse);
  map.endDraw();

  // Subtitle
  subtitle.beginDraw();
  subtitle.background(255);
  if (!currentSubtitle.isEmpty()) {
    subtitle.pushStyle();
    subtitle.fill(0);
    subtitle.textAlign(CENTER, CENTER);
    subtitle.textFont(font);
    subtitle.text(currentSubtitle, subtitle.width/2, subtitle.height /2);
    subtitle.popStyle();
  }
  subtitle.endDraw();

  // Tutorial
  photo.beginDraw();
  if (DEBUGGING) {
    photo.background(255);
  } else {
    photo.background(0);
  }
  photo.image(tutorial, 0, 0, photo.width, photo.height);
  photo.endDraw();

  hernan.beginDraw();
  hernan.background(0);
  if (main.getState() == CHASE) {
    ph.render(hernan);
  }
  hernan.endDraw();

  luciano.beginDraw();
  luciano.background(0);
  if (main.getState() == CHASE) {
    pl.render(luciano);
  }
  luciano.endDraw();



  background(0);

  subtitleSurface.render(subtitle);
  photoSurface.render(photo);
  mapSurface.render(map);

  hernanSurface.render(hernan);
  lucianoSurface.render(luciano);

  resetLastPressed();

  if (DEBUGGING) {
    drawDebuggingText();
  }
}

void drawSubtitle() {
}


void drawDebuggingText() {
  PVector surfaceMouse = mapSurface.getTransformedMouse();

  pushStyle();
  fill(255);
  text("FPS: " + frameRate + "\nmillis: " + millis() + "\nseconds: " + millis() / 1000, 0, 0);
  text("\n\n\n" + surfaceMouse.x + ", " + surfaceMouse.y, 0, 0);
  text("\n\n\n\n" + "state: " + main.getState() + " - step: " + main.getStep() + " - substep:" + main.getSubstep() + " - ending: " + main.getEnding(), 0, 0);
  text("\n\n\n\n\n" + "currentAnswer: " + main.getCurrentAnswer() + "\ncorrectAnswer: " + main.getData().getAnswer() + " - lastAnswerRight?: " + main.getLastAnswerRight(), 0, 0);
  text("\n\n\n\n\n\n\n" + "stepStartTime: " + main.getStepStartTime() / 1000 +
    " - substep 1: " + (main.getStepStartTime() + main.getStepTime() * 0.33) / 1000 +
    " - substep 2: " + (main.getStepStartTime() + main.getStepTime() * 0.66) / 1000 + 
    " - stepTime: " + (main.getStepStartTime() + main.getStepTime()) / 1000, 0, 0);

  popStyle();
}


void reset() {
  main.reset();
  rewindAll();
}

void rewindAll() {
  for (int j = 0; j < trys; j++) {
    for (int i = 0; i < steps; i++) {
      println("audio/" + i + '/' + j + ".wav");
      audios[i][j].pause();
      audios[i][j].rewind();
    }
  }

  endings[0].pause();
  endings[0].rewind();
  endings[1].pause();
  endings[1].rewind();
  confirm.pause();
  confirm.rewind();
}

void keyPressed() {
  switch(key) {
  case 's':
    main.init();
    break;

  case 'r':
    reset();
    break;

  case'y':
    lastPressed = "y";
    break;

  case 'n':
    lastPressed = "n";
    break;

  case 'd': 
    DEBUGGING = !DEBUGGING;
    break;

  case 'c':
    // enter/leave calibration mode, where surfaces can be warped 
    // and moved
    ks.toggleCalibration();
    break;

  case 'l':
    // loads the saved layout
    ks.load();
    break;

  case 'S':
    // saves the layout
    ks.save();
    break;

  default:
    if (main.getData().getInput() == BARCODE) {
      scanner.addLetter(key);
    }
  }
}

void movieEvent(Movie m) {
  m.read();
}
