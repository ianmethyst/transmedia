boolean DEBUGGING = true;

// Input types
static final int NONE = 0;
static final int BOOL = 1;
static final int BARCODE = 2;

// States

static final int STOPPED = -1;
static final int TUTORIAL = 0;
static final int CHASE = 1;
static final int ENDING = 2;

// Endings

static final int UNFINISHED = 0;
static final int BAD = 1;
static final int GOOD = 2;

// Buttons

static final int NO = 0;
static final int YES = 1;

// Colors

final color BLUE = color(0, 0, 255);
final color RED = color(255, 0, 0);
final color ORANGE = color(255, 165, 0);
final color GREEN = color(0, 255, 0);
