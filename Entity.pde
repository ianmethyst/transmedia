class Entity {
  color c;
  int opacity;

  float size;
  static final float maxSize = 150;
  static final float minSize = 25;
  boolean pulseDirection;

  PVector position, oldPosition;
  PVector target;

  boolean moving;
  float startTime;
  float finishTime;

  boolean finished;

  Entity() {
    this(new PVector(72, 296));

    opacity = 0;
    moving = false;
  }

  Entity(PVector position) {    
    this.position = position;
  }

  void update() {
    fade();
    pulse();

    if (moving) {
      position.set(
        map(millis(), startTime, finishTime, oldPosition.x, target.x), 
        map(millis(), startTime, finishTime, oldPosition.y, target.y)
        );

      if (millis() >= finishTime) {
        moving = false;
      }
    }
  }

  void fade() {
    if (!finished && opacity < 255) {
      opacity++;
    } else if (finished && opacity > 0) {
      opacity--;
    }
  }

  void setPos(PVector pos) {
    this.position.set(pos);
  }

  void render(PGraphics surface) {
    surface.pushStyle();
    surface.noStroke();
    if (DEBUGGING) {
      surface.fill(c, 175);
    } else {
      surface.fill(c, opacity);
    }
    surface.ellipse(position.x, position.y, size, size);
    surface.popStyle();
  }

  void pulse() {
    if (pulseDirection && size <= maxSize ) {
      size += 2;
    } else if (!pulseDirection && size >= minSize) {
      size -= 2;
    }

    if (size > maxSize) {
      size = maxSize;
      pulseDirection = false;
    } else if (size < minSize) {
      size = minSize;
      pulseDirection = true;
    }
  }

  void moveTo(PVector target, float finishTime) {
    oldPosition = new PVector();
    oldPosition.set(position);
    println(position);
    println(oldPosition);
    this.target = target;
    startTime = millis();
    this.finishTime = finishTime;
    moving = true;
  }

  void setFinished(boolean finished) {
    this.finished = finished;
  }

  boolean getMoving() {
    return moving;
  }
}

class Police extends Entity {
  // For lerpColor
  float currentColor;
  boolean lerpDirection;

  // For switchColor()
  float lastSecond = 0;

  Police(PVector pos) {
    position = pos;

    c = BLUE;
    currentColor = 0;
    lerpDirection = true;
  }

  void switchColor() {
    float second = millis();

    if (second - lastSecond > 500) {
      if (c == BLUE) {
        c = RED;
      } else {
        c = BLUE;
      }

      lastSecond = second;
    }
  }

  void colorLerp() {
    if (lerpDirection) {
      currentColor += 0.025;
    } else {
      currentColor -= 0.025;
    }

    if (lerpDirection && currentColor > 1) {
      lerpDirection = false;
    } else if (!lerpDirection && currentColor < 0) {
      lerpDirection = true;
    }

    c = lerpColor(BLUE, RED, currentColor);
  }

  @ Override void update() {
    super.update();
    colorLerp();
  }
}

class Hernan extends Entity {
  Hernan (PVector pos) {
    position = pos;

    c = ORANGE;
  }
}

class Luciano extends Entity {
  private int move;
  Luciano(PVector pos) {
    position = pos;
    move = 0;
    c = GREEN;
  }
  
  void setMove(int move) {
    this.move = move;
  }
  
  int getMove() {
    return move;
  }
}
