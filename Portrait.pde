class Portrait {
  color c;
  Portrait(color c) {
    this.c = c;
  }
  
  void render(PGraphics surface) {
    surface.pushStyle();
    surface.fill(c, 170);
    surface.rect(0, 0, surface.height, surface.width);
    surface.popStyle();
  }
}
