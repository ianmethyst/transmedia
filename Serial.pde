static final String findPort() {
  String[] ports = Serial.list();
  //printArray(ports);
  //println(ports.length);

  for (String p : ports) {
    for (int i = 0; i <= 5; ++i) {
      if (p.equals("COM" + i) || p.equals("/dev/ttyACM" + i)) {
        println("Using port " + p);
        return p;
      }
    }
}

  println("No serial port available");
  return null;
}

void readSerial() {
  while (serialPort != null && serialPort.available() > 0) {
    serialString = serialPort.readStringUntil(LF);

    if (serialString != null) {
      print(Serial.list()[0] + ": " + serialString);

      if (serialString.equals("y\r\n")) {
        lastPressed = "y";
      } else if (serialString.equals("n\r\n")) {
        lastPressed = "n";
      }
    }
  }
}

void resetLastPressed() {
  lastPressed = "";
}
