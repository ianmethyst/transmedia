class Scanner {
  String characters;
  int last;

  Scanner() {
    reset();
  }

  String checkReturn() {
    if (characters.length() >= 1) {
      if (characters.charAt(last) == ENTER) {
        String copy = characters.substring(0, characters.length() - 1);
        println(copy);
        reset();
        return copy;
      }
    }
    return "";
  }

  void reset() {
    characters = "";
  }

  void addLetter(char k) {
    characters = characters + k;
    last = characters.length() - 1;
  }
}
