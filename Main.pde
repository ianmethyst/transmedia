class Main {
  private int state;
  private int ending;

  private PImage map;

  private Police police;
  private Luciano luciano;
  private Hernan hernan;

  private JSONLoader j;
  private boolean tutorialPlayed;

  private boolean lastAnswerRight;
  private String currentAnswer;
  private int subStep;
  private int stepStartTime;
  private int stepTime;
  private int step;
  private int rightAnswers;


  Main(int width, int height) {
    map = loadImage("data/map.png");
    map.resize(width, height);

    reset();
  }

  void reset() {
    step = 0;
    subStep = 0;
    currentAnswer = "";
    lastAnswerRight = false;
    police = null;
    luciano = null;
    hernan = null;
    state = STOPPED;
    tutorialPlayed = false;
    loadData(step);
    rewindAll();
  }


  void loadData(int step) {
    j = new JSONLoader(step);
  }

  void update() {
    switch(state) {
    case STOPPED: 
      while (tutorial.time() > 0) {
        tutorial.jump(0);
      }

      if (lastPressed == "y") {
        main.init();
      }


      if (tutorialPlayed && tutorial.time() == 0) {
        tutorialPlayed = false;
        tutorial.pause();
      }
      break;

    case TUTORIAL: 
      if (!tutorialPlayed) {
        tutorialPlayed = true;
        tutorial.play();
        SindicaSubs.setCurrentSub(0);
        currentSubtitle = SindicaSubs.getText(SindicaSubs.getCurrentSub());
        SindicaSubs.setFirstSubTime(millis());
      }

      if (SindicaSubs.getCurrentSub() + 1 < SindicaSubs.getSubsSize()) { 
        if (millis() > SindicaSubs.getFirstSubTime() + SindicaSubs.getTime(SindicaSubs.getCurrentSub() + 1)) {
          SindicaSubs.setCurrentSub(SindicaSubs.getCurrentSub() + 1);
          currentSubtitle = SindicaSubs.getText(SindicaSubs.getCurrentSub());
        }
      }

      if ((int) tutorial.time() == (int) tutorial.duration()) {
        currentSubtitle = "";
        state = CHASE;
      }
      break;

    case CHASE:
      createEntities();

      switch (subStep) {
      case 0:
        if ( step > 0) {
          loadData(step);
        }
        confirm.pause();
        confirm.rewind();
        audios[step][0].play();
        currentSubtitle = j.getSubtitle(0);

        subStep++;
        stepStartTime = millis();
        stepTime = lastAnswerRight ? 25 * 1000 : 30 * 1000;

        if (j.getSubtarget() != null) {
          police.moveTo(j.getSubtarget(), stepStartTime + (stepTime * 0.5));
          println("moving to subtarget");
        } else {
          println("moving to target directly");
          police.moveTo(j.getTarget(), stepStartTime + stepTime);
        }
        break;

      case 1:
        if (millis() > stepStartTime + stepTime * 0.33 ) {
          if (currentAnswer.isEmpty()) {
            audios[step][1].play();
            currentSubtitle = j.getSubtitle(1);
          }
          subStep++;
        }
        break;

      case 2:
        if (millis() > stepStartTime + stepTime * 0.66 ) {
          if (currentAnswer.isEmpty()) {
            audios[step][2].play();
            currentSubtitle = j.getSubtitle(2);
          }
          subStep++;
        }
        break;

      case 3:
        if (millis() > stepStartTime + stepTime) {
          if (!currentAnswer.isEmpty()) {
            if (currentAnswer.equals(j.getAnswer())) {
              lastAnswerRight = true;
              rightAnswers++;
            } else {
              lastAnswerRight = false;
            }

            if (!j.getLastAnswer()) {
              step++;
              subStep = 0;
              currentAnswer = "";
            } else {
              stepStartTime = millis();
              police.setFinished(true);
              luciano.setFinished(true);
              hernan.setFinished(true);

              if (rightAnswers > 2) {
                ending = BAD;                   
                println("reached BAD ending");
              } else {
                ending = GOOD;
                println("reached GOOD ending");
              }

              state = ENDING;
            }
          } else if (currentAnswer.isEmpty()) {
            println("reached UNFINISHED ending");
            police.setFinished(true);
            luciano.setFinished(true);
            hernan.setFinished(true);

            stepStartTime = millis();
            ending = UNFINISHED;
            state = ENDING;
          }
        }
        break;
      }

      checkAnswers();

      if (!luciano.getMoving()) {
        println(luciano.getMove());

        switch(luciano.getMove()) {
        case 0:
          println("move 0");
          luciano.moveTo(new PVector(1533, 1169), millis() + 1000 * 30);
          luciano.setMove(luciano.getMove() + 1);
          break;
        case 1:
          println("move 1");
          luciano.moveTo(new PVector(1549, 948), millis() + 1000 * 30);
          luciano.setMove(luciano.getMove() + 1);
          break;
        case 2:
          luciano.moveTo(new PVector(1204, 659), millis() + 1000 * 60);
          luciano.setMove(luciano.getMove() + 1);
          break;
        case 3:
          luciano.moveTo(new PVector(1274, 587), millis() + 1000 * 15);
          luciano.setMove(luciano.getMove() + 1);
          break;
        case 4:
          luciano.moveTo(new PVector(1132, 469), millis() + 1000 * 22.5);
          luciano.setMove(luciano.getMove() + 1);
          break;
        case 5:
          luciano.moveTo(new PVector(966, 459), millis() + 1000 * 12.5);
          hernan.moveTo(new PVector(966, 459), millis() + 1000 * 12.5);
          luciano.setMove(luciano.getMove() + 1);
          break;
        case 6:
          state = ENDING;
          ending = GOOD;
        }
      }

      if (!police.getMoving()) {
        println("moving to target");
        police.moveTo(j.getTarget(), stepStartTime + stepTime);
      }

      police.update();
      hernan.update();
      luciano.update();
      break;
    case ENDING: 
      {
        if (ending == BAD) {
          endings[0].play();
        } else if (ending == GOOD) {
          endings[1].play();
        }

        police.update();
        hernan.update();
        luciano.update();

        reset();
      }
    }
  }

  void createEntities() {
    if (police == null && hernan == null && luciano == null) {
      police = new Police(new PVector(205, 1098));
      luciano = new Luciano(new PVector(1411, 1300));
      hernan = new Hernan(new PVector(1096, 498));
    }
  }

  void checkAnswers() {
    // Get answer for step
    if (currentAnswer.isEmpty()) {
      switch (j.getInput()) {
      case BOOL:
        if (lastPressed.equals("y") || lastPressed.equals("n")) {
          //rewindAll();
          confirm.play();
          currentSubtitle = "Recibido";
          currentAnswer = lastPressed;
        }
        break;

      case BARCODE: 
        String scan = scanner.checkReturn(); 
        if (!scan.isEmpty()) {
          //rewindAll();
          confirm.play();
          currentSubtitle = "Recibido";
          currentAnswer = scan;
        }
        break;
      }
    }
  }

  void init() {
    switch (state) {
    case STOPPED:
      state = TUTORIAL;
      break;
    case TUTORIAL:
      tutorial.jump(tutorial.duration() - 1);
      break;
    }
  }

  void render(PGraphics surface) {
    if (DEBUGGING) {
      surface.image(map, 0, 0);
    }
    if (police != null && hernan != null && luciano != null) {
      police.render(surface);
      hernan.render(surface);
      luciano.render(surface);
    }
  }

  int getState() {
    return state;
  }

  int getStep() {
    return step;
  }

  String getCurrentAnswer() {
    return currentAnswer;
  }

  int getSubstep() {
    return subStep;
  }

  int getStepStartTime() { 
    return stepStartTime;
  }

  int getStepTime() { 
    return stepTime;
  }

  int getEnding() { 
    return ending;
  }

  boolean getLastAnswerRight() {
    return lastAnswerRight;
  }

  JSONLoader getData() {
    return j;
  }
}
