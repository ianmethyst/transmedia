class JSONLoader {
  JSONArray steps = loadJSONArray("steps.json");

  int step, input;
  boolean lastAnswer;
  String rightAnswer;
  String[] subtitles;
  PVector target, subtarget;
  //int time; 
  //String audio;

  JSONLoader(int step) {
    this.step = step;

    subtitles = new String[3];

    JSONObject data = steps.getJSONObject(step);  
    JSONObject target = data.getJSONObject("target");
    if (!data.isNull("subtarget")) {
      JSONObject subtarget = data.getJSONObject("subtarget");
      this.subtarget = new PVector(subtarget.getInt("x"), subtarget.getInt("y"));
    }

    JSONArray subs = data.getJSONArray("subs");

    for (int i = 0; i < 3; i++) {
      JSONObject currentSub = subs.getJSONObject(i);

      subtitles[i] = currentSub.getString("text");
    }

    if (!data.isNull("lastAnswer")) {
      lastAnswer = true;
    } else {
      lastAnswer = false;
    }


    this.target = new PVector(target.getInt("x"), target.getInt("y"));

    input = data.getInt("inputType");
    rightAnswer = data.getString("rightAnswer");

    printDebuggingInfo();
  }

  public int getStep() { 
    return step;
  }

  public boolean getLastAnswer() {
    return lastAnswer;
  }

  public PVector getTarget() { 
    return target;
  }

  public PVector getSubtarget() { 
    return subtarget;
  }

  //public int getTime() { 
  //  return time;
  //}

  public int getInput() { 
    return input;
  }

  public String getAnswer() { 
    return rightAnswer;
  }

  String getSubtitle(int s) {
    return subtitles[s];
  }

  //public String getAudio() { 
  //  return audio;
  //}

  private void printDebuggingInfo() {
    //println("step: " + step + " - x: " + x + " - y: " + " - time: " + time + " - audio: " + audio + " - input: " + input);
    println("step: " + step + " - x: " + target.x + " - y: " + target.y  +" - input: " + input);
    if (subtarget != null) {
      println("subtarget x: " + subtarget.x + " - subtarget y: " + subtarget.y);
    }
  }
}
